import importlib.metadata

import my_project as m


def test_version():
    assert importlib.metadata.version("my_project") == m.__version__
